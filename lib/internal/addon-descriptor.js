const utils = require("./utils");

module.exports = function (addon) {
  const firstPass = utils.replaceTokensInJson(
    utils.loadJSON(addon.descriptorFilename),
    "{{localBaseUrl}}",
    addon.config.localBaseUrl()
  );
    console.log("MIRO firstPass", firstPass);
  const secondPass = utils.replaceTokensInJson(
    firstPass,
    "{{environment}}",
    addon.config.environment()
  );
  const thirdPass = utils.replaceTokensInJson(
    secondPass,
    "{{appKey}}",
    addon.config.appKey()
  );

  let finalResult = thirdPass;
  if (typeof addon.config.descriptorTransformer === "function") {
      console.log("MIRO thirdPass");
    finalResult = addon.config.descriptorTransformer()(thirdPass, addon.config);
  }
  console.log("MIRO finalResult", finalResult);
  return finalResult;
};

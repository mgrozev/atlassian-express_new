module.exports = function (addon) {
  const product = addon.config.product();

  if (product.isJIRA || product.isConfluence) {
      console.log("MIRO HOST is JIRA REGISTRATIN");
    return require("./register-jira-conf");
  } else if (product.isBitbucket) {
      console.log("MIRO HOST is NOT JIRA REGISTRATIN");
    return require("./register-bitbucket");
  } else {
    throw new Error(`Not sure how to register against ${product}`);
  }
};
